set nocompatible " be iMproved
filetype off " required

" ----------------
" === Vim-Plug ===
" ----------------

" Automatic Vim-Plug installation
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
" The default directory is : '~/.vim/plugged'

" --- NERDTree ---
" File system explorer
Plug 'preservim/nerdtree' " https://github.com/preservim/nerdtree
" Add icons for NerdTree
Plug 'ryanoasis/vim-devicons' " https://github.com/ryanoasis/vim-devicons
" Add syntax highlighting for filetype
Plug 'tiagofumo/vim-nerdtree-syntax-highlight' " https://github.com/tiagofumo/vim-nerdtree-syntax-highlight

" --- NERDCommenter ---
" Comment functions
" Plug 'preservim/nerdcommenter' " https://github.com/preservim/nerdcommenter

" --- Vim Airline ---
" Better status line
Plug 'vim-airline/vim-airline' " https://github.com/vim-airline/vim-airline
" Lots of themes for airline
Plug 'vim-airline/vim-airline-themes'

" --- ALE ---
" Provides linting (syntax checking and semantic errors)
Plug 'dense-analysis/ale' " https://github.com/dense-analysis/ale

" -- Autocompete (CoC)
" Provides autocompletion thanks to LSP
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" --- CMake ---

" Simple CMake integration
" Plug 'cdelledonne/vim-cmake' " https://github.com/cdelledonne/vim-cmake

" Complex CMake integration
" Plug 'ilyachur/cmake4vim' " https://github.com/ilyachur/cmake4vim

" --- Rust ---
" Provides:
" - Rust file detection
" - Syntax highlighting
" - Formatting
" - Syntastic integration
" - And more...
" Requires:
" - Vim 8+
Plug 'rust-lang/rust.vim' " https://github.com/rust-lang/rust.vim

" Python Jedi
Plug 'davidhalter/jedi-vim' " https://github.com/davidhalter/jedi-vim

" Initialize plugin system
" - Automatically executes `filetype plugin indent on` and `syntax enable`.
call plug#end()

" -----------------
" === Functions ===
" -----------------

function! Formatonsave()
  let l:formatdiff = 1
  py3f /usr/share/clang/clang-format.py
endfunction

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" -----------------
" === Variables ===
" -----------------

" Automatic running of `:RustFmt` when saving a buffer.
let g:rustfmt_autosave = 1

" Airline Theme
let g:airline_theme='dark'

" ALE - Always show the sign gutter
let g:ale_sign_column_always = 1

" ALE - Set rust linters
let g:ale_linters = {
	\ 	'ada' : ['ada_language_server'],
	\	'cpp' : ['clang'],
	\	'python' : ['flake8', 'pylint' ],
	\	'rust' : ['analyzer', 'cargo', 'rustc' ],
	\}

" ALE - Enable autocompletion
" set completeopt=menu,menuone,preview,noselect,noinsert
" let g:ale_completion_enabled = 1
" set omnifunc=ale#completion#OmniFunc

" ALE - Change error sign
let g:ale_sign_error = '●'

" -------------------
" === Autocommands ===
" -------------------

" Start NERDTree. If a file is specified, move the cursor to its window.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif

" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" Python indentation
au BufNewFile,BufRead *.py
    \ set expandtab       " replace tabs with spaces
    \ set autoindent      " copy indent when starting a new line
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4


" Apply clang-format on buffer save
" autocmd BufWritePre *.h,*.hpp,*.c,*.cpp call Formatonsave()
autocmd BufWritePre *.hpp,*.cpp call Formatonsave()

" ----------------
" === Commands ===
" ----------------

" Create the `tags` file
command! MakeTags !ctags -R .

" ----------------
" === Inoremap ===
" ----------------

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"


" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice.
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" -----------------
" === Vim Setup ===
" -----------------

" force encoding to UTF-8
" set encoding=utf-8

" show line number
set number

" show more info on cursor position
set ruler

" Display incomplete commands
set showcmd

" Don't wrap lines
set nowrap

" Highlight cursor line underneath the cursor horizontally
" set cursorline

" Highlight the x color to limit code line size
set colorcolumn=100

" Tab & Spaces width
set tabstop=4
set softtabstop=4
set shiftwidth=4
" set noexpandtab
set expandtab

" Apply best tabs by looking at your current context
set smarttab

" Search down into subfolders
set path+=**
" Highlight matches
set hlsearch
" Incremental searching
set incsearch
" Display all matching files when we tab complete
set wildmenu

" Searches are case insensitive ...
set ignorecase

" ... unless they contain at least one capital letter
set smartcase

" Show hidden chars
set listchars=tab:▸\ ,trail:•,extends:❯,precedes:❮,nbsp:+
set list
set showbreak=↪\

" Always return when using backspace
set backspace=indent,eol,start

" Set mouse click to append mode
set mouse=a
