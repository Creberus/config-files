#!/usr/bin/env bash
set -euo pipefail

SCRIPT_NAME="$0"

HELPER_TEXT="Usage: $SCRIPT_NAME [-f] [-h]\n\n\
    Flags:\n\
    \t-h\tPrint this helper.\n
    \t-f\tForce the file to be overwritten in the home directory.\n"

# Define the option string with the arg names
optstring=":fh"

# Parsing arguments
while getopts ${optstring} arg; do
    case ${arg} in
        f)
            # TODO Set force overwrite flag
            ;;
        h)
            echo -e "$HELPER_TEXT"
            exit 0
            ;;
        ?)
            echo -e "Invalid option: -${OPTARG}."
            echo -e "$HELPER_TEXT"
            exit 2
            ;;
    esac
done

# Copy the Xresources configuration file
XRESOURCES_HOME_PATH="$HOME/.Xresources"
if [[ -e $XRESOURCES_HOME_PATH ]]; then
    echo -e "Configuration file '.Xresources' already exists in '$HOME' directory."
else
    cp "Xresources-config" "$XRESOURCES_HOME_PATH"
    echo -e "Copying .Xresources configuration file..."
fi
